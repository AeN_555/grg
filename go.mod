module gitlab.com/AeN_555/grg

go 1.16

require (
	github.com/pierrec/lz4/v3 v3.3.2
	gitlab.com/AeN_555/colorlog v1.2.0
)
