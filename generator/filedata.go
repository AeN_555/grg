package generator

import (
	"bytes"
	"fmt"
	"github.com/pierrec/lz4/v3"
	log "gitlab.com/AeN_555/colorlog"
	"gitlab.com/AeN_555/grg/settings"
	"io"
	"os"
	"path/filepath"
)

type (
	// fileData contains the data of a file that need to be templated for generation.
	fileData struct {
		commonData
		minimumData

		Compressed bool
		Data       string
	}
)

// To create a new fileData based on the file path and its directory path.
func newFileData(filePath string, directoryPath string) *fileData {
	log.Debug("newFileData(filePath=\"", filePath, "\", directoryPath=\"", directoryPath, "\")")
	res := fileData{commonData: *newCommonData()}

	res.PackageName = filepath.Base(settings.PathToGeneratedDirectory)

	newRelativePath := relativePath(filePath, directoryPath)
	res.Name = formatName(filepath.Base(newRelativePath))
	res.CheckSum = generateId(newRelativePath)

	res.Compressed = settings.UseCompression

	res.Data = rawData(filePath, res.Compressed)

	return &res
}

// To get the raw data of a file after compression (if needed).
func rawData(filePath string, compressed bool) string {
	res, err := os.ReadFile(filePath)
	if err != nil {
		log.Error(err, "Failed to read file \"", filePath, "\"")
	}

	if compressed == true {
		var buff bytes.Buffer
		lz4w := lz4.NewWriter(&buff)
		if _, err = io.Copy(lz4w, bytes.NewReader(res)); err != nil {
			log.Error(err, "Failed to compress \"", filePath, "\"")
		}
		if err = lz4w.Close(); err != nil {
			log.Error(err, "Failed to flush lz4.Writer for \"", filePath, "\"")
		}
		res = buff.Bytes()
	}

	return fmt.Sprintf("%q", string(res))
}
