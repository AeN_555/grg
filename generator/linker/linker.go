// Package linker provides generation of links between files and directories.
//
// To do so, the generation creates the variable of the root directory and links every file through its directories
// with the right constant for it.
package linker

import (
	log "gitlab.com/AeN_555/colorlog"
	"gitlab.com/AeN_555/grg/settings"
	"os"
	"text/template"
)

type (
	// directory contains all directory data to allow templated generation for the linker.
	directory struct {
		file
		NextDirectories map[string]*directory
		NextFiles       []*file
	}

	// file contains all file data to allow templated generation for the linker.
	file struct {
		Name     string
		CheckSum uint64
	}
)

const (
	txtAppendRoot string = `
var (
{{.Name}} directory{{.CheckSum}} = directory{{.CheckSum}}{
{{- template "appendDirectory" .}}
}
)
`

	txtAppendDirectory string = `{{- range .NextDirectories}}
{{.Name}}: directory{{.CheckSum}}{
{{- template "appendDirectory" .}}
},
{{- end -}}
{{- range .NextFiles}}
{{template "appendFile" .}}
{{- end -}}`

	txtAppendFile string = `{{.Name}}: &file{{.CheckSum}},`
)

var (
	root *directory = &directory{}

	tplAppend *template.Template = template.Must(template.New("appendRoot").Parse(txtAppendRoot))
)

// To initialize the package.
func init() {
	template.Must(tplAppend.New("appendDirectory").Parse(txtAppendDirectory))
	template.Must(tplAppend.New("appendFile").Parse(txtAppendFile))
}

// To reset the linker to its default value.
func Reset() {
	root = &directory{}
}

// To add a file to the linker for generation.
func AddFile(directoryList []string, fileName string, checkSum uint64) {
	log.Debug("AddFile(directoryList=", directoryList, ", fileName=", fileName, ")")
	defer log.Debug("AddFile -- end")

	d := root
	for _, dirName := range directoryList[1:] {
		d = d.NextDirectories[dirName]
	}
	d.NextFiles = append(d.NextFiles, &file{Name: fileName, CheckSum: checkSum})
}

// To add a directory to the linker for generation.
func AddDirectory(directoryList []string, fileName string, checkSum uint64) {
	log.Debug("AddDirectory(directoryList=", directoryList, ", fileName=", fileName, ")")
	defer log.Debug("AddDirectory -- end")

	if len(directoryList) < 1 {
		root.Name = fileName
		root.CheckSum = checkSum
	} else {
		d := root
		for _, dirName := range directoryList[1:] {
			d = d.NextDirectories[dirName]
		}
		if d.NextDirectories == nil {
			d.NextDirectories = make(map[string]*directory)
		}
		d.NextDirectories[fileName] = &directory{file: file{Name: fileName, CheckSum: checkSum}}
	}
}

// To generate all links for all files in the linker.
func LinkFiles() {
	newPath := settings.PathToGeneratedDirectory + string(os.PathSeparator) + root.Name + ".go"
	log.Verbose("Appending links in file \"", newPath, "\"")
	file, err := os.OpenFile(newPath, os.O_APPEND|os.O_WRONLY, 0)
	if err != nil {
		log.Error(err, "Failed to append file \"", newPath, "\"")
	}
	defer file.Close()

	if err := tplAppend.ExecuteTemplate(file, "appendRoot", root); err != nil {
		log.Error(err, "Failed to generate \"append\" template in \"", newPath, "\"")
	}
	log.Verbose("Links added to file \"", newPath, "\"")
}
