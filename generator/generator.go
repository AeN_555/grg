// Package generator provides a generation of file as static ressource for Go package.
package generator

import (
	log "gitlab.com/AeN_555/colorlog"
	"gitlab.com/AeN_555/grg/errors"
	"gitlab.com/AeN_555/grg/settings"
	"os"
	"path/filepath"
	"strings"
	"unicode"
)

const (
	newDirectorySeparator string = "."
	newExtensionSparator  string = "_"
)

// To convert a file path with a directory path to relatve path.
//
// It supposes that the file path must contains the directory path. Otherwise, result is ill-formated.
func relativePath(filePath string, directoryPath string) string {
	if directoryPath != "." {
		filePath = filePath[len(directoryPath)+1:]
	}
	return filePath
}

// To capitalize the first letter of a string.
func upperFirst(str string) string {
	res := []rune(str)
	res[0] = rune(unicode.ToUpper(rune(str[0])))
	return string(res)
}

// To check and format a file name to a Go variable name.
func formatName(fileName string) string {
	log.Debug("formatName(fileName=\"", fileName, "\")")

	fileName = filepath.Base(fileName)
	checkName(fileName)

	ext := filepath.Ext(fileName)
	if len(ext) > 1 {
		ext = strings.ToUpper(ext[1:])
	} else {
		ext = ""
	}

	fileName = strings.ReplaceAll(fileName, ".", newExtensionSparator)
	fileName = strings.ReplaceAll(fileName, " ", newExtensionSparator)
	fileName = fileName[:len(fileName)-len(ext)] + ext
	fileName = upperFirst(fileName)

	log.Debug("formatName - end with \"", fileName, "\"")
	return fileName
}

// To check if the file name can be converted to a Go variable.
func checkName(fileName string) {
	if unicode.IsLetter(rune(fileName[0])) == false {
		log.Error(errors.NumberAtBeginning, "Failed to convert \"", fileName, "\" to ressource")
	}

	for _, c := range fileName {
		if unicode.IsSpace(c) == true {
			log.Error(errors.NoSpacer, "Failed to convert \"", fileName, "\" to ressource")
		}

		if unicode.IsSymbol(c) == true {
			log.Error(errors.NoSymbol(c), "Failed to convert \"", fileName, "\" to ressource")
		}
	}
}

// To convert a path to a list of directories.
func pathToDirectoryArray(filePath string) []string {
	res := strings.Split(filePath, string(os.PathSeparator))
	res = res[:len(res)-1] // discard the file name

	for index, dir := range res {
		res[index] = upperFirst(dir)
	}

	return res
}

// To generate the new file that will contain the static ressource.
func generateNewFile(directories []string, fileName string) *os.File {
	newPath := settings.PathToGeneratedDirectory + string(os.PathSeparator)
	for _, name := range directories {
		newPath = newPath + name + newDirectorySeparator
	}
	newPath = newPath + fileName + ".go"

	if _, err := os.Stat(newPath); err == nil {
		log.Error(errors.Collision, "File \"", newPath, "\" already exist")
	} else if os.IsNotExist(err) == true {
		log.Verbose("Creating file \"" + newPath + "\"")
		file, err := os.Create(newPath)
		if err != nil {
			log.Error(err, "Failed to create file \"", newPath, "\"")
		}
		return file
	} else {
		log.Error(err, "Failed to check file \"", newPath, "\"")
	}

	return nil
}
