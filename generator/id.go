package generator

import (
	"hash/crc64"
)

const (
	crc64ecmaReversed uint64 = 0xC96C5795D7870F42
)

var (
	crc64Table = crc64.MakeTable(crc64ecmaReversed)
)

// To generate a unique ID based on the CRC64 checksum of a path.
func generateId(path string) uint64 {
	return crc64.Checksum([]byte(path), crc64Table)
}
