package generator

type (
	// minimumData contains the minimum fields to any file or directory that need to be templated for generation.
	minimumData struct {
		Name     string
		CheckSum uint64
	}
)
