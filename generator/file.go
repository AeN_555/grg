package generator

import (
	log "gitlab.com/AeN_555/colorlog"
	"gitlab.com/AeN_555/grg/generator/linker"
	"text/template"
)

const (
	txtFile string = `// Generated file by grg (see https://gitlab.com/AeN_555/grg)
//
// DO NOT MODIFY OR VERSION-CONTROL IT
package {{.PackageName}}

var (
	// File named "{{.Name}}"
	file{{.CheckSum}} File = File{
		compressed: {{.Compressed}},
		data:       []byte({{.Data}}),
	}
)
`
)

var (
	tplFile *template.Template = template.Must(template.New("file").Parse(txtFile))
)

// To generate a new file that contains the static ressource of a file based on its file path and its directory path.
func GenerateFile(filePath string, directoryPath string) {
	log.Debug("GenerateFile(filePath=\"", filePath, "\", directoryPath=\"", directoryPath, "\")")
	defer log.Debug("GenerateFile -- end")

	data := newFileData(filePath, directoryPath)

	dirArray := pathToDirectoryArray(relativePath(filePath, directoryPath))
	linker.AddFile(dirArray, data.Name, data.CheckSum)

	file := generateNewFile(dirArray, data.Name)
	defer file.Close()

	if err := tplFile.ExecuteTemplate(file, "file", data); err != nil {
		log.Error(err, "Failed to generate \"file\" template in \"", file.Name(), "\"")
	}
}
