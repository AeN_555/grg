package generator

import (
	log "gitlab.com/AeN_555/colorlog"
	"gitlab.com/AeN_555/grg/generator/linker"
	"text/template"
)

const (
	txtDirectory string = `// Generated file by grg (see https://gitlab.com/AeN_555/grg)
//
// DO NOT MODIFY OR VERSION-CONTROL IT
package {{.PackageName}}

type (
	// Directory named "{{.Name}}"
	directory{{.CheckSum}} struct {
	{{- range .ContentDirectories}}
		{{.Name}} directory{{.CheckSum}}
	{{- end}}
	{{- range .ContentFiles}}
		{{.Name}} *File
	{{- end}}
	}
)
`
)

var (
	tplDirectory *template.Template = template.Must(template.New("directory").Parse(txtDirectory))
)

// To generate a new file that represents a directory based on its file path and its directory path.
func GenerateDirectory(filePath string, directoryPath string) {
	log.Debug("GenerateDirectory(filePath=\"", filePath, "\", directoryPath=\"", directoryPath, "\")")
	defer log.Debug("GenerateDirectory -- end")

	data := newDirectoryData(filePath, directoryPath)

	dirArray := pathToDirectoryArray(relativePath(filePath, directoryPath))
	linker.AddDirectory(dirArray, data.Name, data.CheckSum)

	file := generateNewFile(dirArray, data.Name)
	defer file.Close()

	if err := tplDirectory.ExecuteTemplate(file, "directory", data); err != nil {
		log.Error(err, "Failed to generate \"directory\" template in \"", file.Name(), "\"")
	}
}
