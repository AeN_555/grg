package generator

import (
	log "gitlab.com/AeN_555/colorlog"
	"os"
	"path/filepath"
)

type (
	// directoryData contains the data of a directory that need to be templated for generation.
	directoryData struct {
		commonData
		minimumData

		ContentDirectories []*minimumData
		ContentFiles       []*minimumData
	}
)

// To create a new directoryData based on the file path and its directory path.
func newDirectoryData(filePath string, directoryPath string) *directoryData {
	log.Debug("newDirectoryData(filePath=\"", filePath, "\", directoryPath=\"", directoryPath, "\")")
	res := directoryData{commonData: *newCommonData()}

	newRelativePath := relativePath(filePath, directoryPath)

	res.Name = formatName(filepath.Base(newRelativePath))
	res.CheckSum = generateId(newRelativePath)

	files, err := os.ReadDir(filePath)
	if err != nil {
		log.Error(err, "Failed to read directory \"", filePath, "\"")
	}
	for _, f := range files {
		data := minimumData{
			Name:     formatName(f.Name()),
			CheckSum: generateId(newRelativePath + string(os.PathSeparator) + f.Name()),
		}
		if f.IsDir() == true {
			res.ContentDirectories = append(res.ContentDirectories, &data)
		} else {
			res.ContentFiles = append(res.ContentFiles, &data)
		}
	}

	log.Debug("newDirectoryData - end with ", res)
	return &res
}
