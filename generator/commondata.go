package generator

import (
	log "gitlab.com/AeN_555/colorlog"
	"gitlab.com/AeN_555/grg/settings"
	"path/filepath"
)

type (
	// commonData contains the common data of any structure that will be used as template for generation.
	commonData struct {
		PackageName string
	}
)

// To create a new commonData.
func newCommonData() *commonData {
	log.Debug("newBaseData()")

	var res commonData
	res.PackageName = filepath.Base(settings.PathToGeneratedDirectory)

	log.Debug("newBaseData -- end with ", res)
	return &res
}
