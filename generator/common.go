package generator

import (
	log "gitlab.com/AeN_555/colorlog"
	"os"
	"text/template"
)

const (
	txtCommon string = `// Generated file by grg (see https://gitlab.com/AeN_555/grg)
//
// DO NOT MODIFY OR VERSION-CONTROL IT
package {{.PackageName}}

import (
	"bytes"
	"github.com/pierrec/lz4/v3"
	"io"
)

type (
	File struct {
		compressed bool
		data       []byte
	}
)

func (f *File) RawData() ([]byte, error) {
	if f.compressed == false {
		return f.data, nil
	}

	var res bytes.Buffer
	if _, err := io.Copy(&res, lz4.NewReader(bytes.NewReader(f.data))); err != nil {
		return nil, err
	}
	return res.Bytes(), nil
}
`
)

var (
	tplCommon *template.Template = template.Must(template.New("common").Parse(txtCommon))
)

// To generate the unique common file with constant data in it to be able to use the generate data.
func GenerateCommonFile(filePath string) {
	data := newCommonData()

	log.Verbose("Creating file \"" + filePath + "\"")
	file, err := os.Create(filePath)
	if err != nil {
		log.Error(err, "Failed to create file \"", filePath, "\"")
	}
	defer file.Close()

	tplCommon.ExecuteTemplate(file, "common", data)
}
