// Package errors provides custom errors for grg packages.
package errors

import (
	"errors"
)

var (
	NumberAtBeginning error = errors.New("grg: 1st character must be a letter")
	NoSpacer          error = errors.New("grg: spacer is not allowed")
	Collision         error = errors.New("grg: collision detected, 2 files or directories have the same path (first letter is not case sensitive)")
)

// To get a custom error about a not allowed symbol.
func NoSymbol(character rune) error {
	return errors.New("grg: symbol '" + string(character) + "' is not allowed")
}
