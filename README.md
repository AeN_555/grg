# grg (Go Ressource Generator)

![Logo](./logo.png "grg logo")

grg is a Go program to generate static ressources for other Go binaries.

![Example of generation with grg](./example.png)

### Requierements

The package depends on [ColorLog](https://gitlab.com/AeN_555/colorlog "Link to ColorLog").

Obviously, it also requires [Go](https://golang.org/ "Link to Golang") itself.
But, at least, version 1.4 to be able to run `go generate` command.

### Installation

grg uses Go module system, so you don't need to install it to be able to use it.
Just add the necessary comment in your main package like in the example below.

### Example

![Example project](./tree.png)

In "./res/data.txt":

```
A bunch of data in any form: α and ω
```

In "./myProject.go":

```go
package main

import (
	"myProject/gen"
	"fmt"
)

//go:generate go run gitlab.com/AeN_555/grg
func main() {
	data, err := gen.Res.Data_TXT.RawData()
	if err != nil {
		fmt.Println("Error: ", err)
		return
	}
	fmt.Println("Content of ./res/data.txt (at build time): ", string(data))
}
```

### Build and result

```bash
$ go generate
```

![Result of the generation](./resultGeneration.png)

```bash
$ go build .
$ ./myProject
```

![Result of the example](./result.png)

### Usage

```
Usage of grg:
  -c    enable color (default true)
  -d string
        the directory to generate in (default "./gen")
  -r value
        the directories to look for ressources
  -v uint
        verbose level (0 to 5) (default 3)
  -z    use compression (lz4) to store data (default true)
```

If you can have multiple ressources directories so you can add them all like that:

```bash
grg -r=./resDir1 -r=./resDir2
```

If no directory is specified, default is "./res"

Notice that you can use thoses flags in the `go:generate` comment to specify your project.

### Warnings and cautions

grg uses a specified directory to generate code so this directory will be completly erased at start-up (usually when `go generate` is run).

There is some collisions possible between files due to the file system and Go naming permissions. Some charaters in the file names can also be forbidden. If it happen, the generator will fail and show the conflict. It should be rare but at the end, you will always be notified if something bad happen.

### Questions and answer

 - *Why generate code to embed ressources ?*

   - Before Go 1.16 : I didn't know an other way to do such thing. Apparently there was none.
   - Since Go 1.16 : Go team releases [an embed package](https://pkg.go.dev/embed "Link to embeded package documentation") and I encourage anyone to use it. From now, grg program is a legacy tool. It will continue to work because it may solve some specific and unused cases but, I believe the embeded package can do anything grg is doing.

 - *Which is the main inspiration source for this project ?*

QRessource (from [Qt](https://www.qt.io/ "Link to Qt website")) was my main source of inspiration. I had a very good experience with it and I tried to produce a result as simple to use as this last one.

 - *Why "go generate" ?*

Because it's the official generator for Go and this program want to follow Go idioms as much as possible. But, at the end, it's a standalone binary so you can use it anyhow you want.

 - *Why lz4 compression ?*

lz4 is the fastest decompression algorithm so it's the best for runtime. I prefere to increase build time than runtime.

 - *Why an other Go static ressource generator ?*

Because many are deprecated and don't use Go module system. Moreover, some are very complexe, some are Web oriented and others very unfriendly to use. This one is very simple to use and it can be used for any project regardless of its goals.
