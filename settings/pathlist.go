package settings

import (
	"fmt"
)

type (
	// PathList is a list of path.
	PathList []string
)

// To get a string that represent the variable.
func (pl *PathList) String() string {
	return fmt.Sprintf("%v", *pl)
}

// To set a string in the variable.
func (pl *PathList) Set(value string) error {
	*pl = append(*pl, value)
	return nil
}
