// Package settings provides a setting system with any parameters necessary to configure the runtime.
//
// All thoses parameters can be changed with flags when the binray is run.
package settings

import (
	"flag"
	log "gitlab.com/AeN_555/colorlog"
	"os"
	"path/filepath"
)

var (
	PathToRessourceDirectories PathList
	PathToGeneratedDirectory   string
	UseCompression             bool
	VerboseLevel               uint
	Color                      bool
)

// To parse the flags in the command line and apply thoses settings to the runtime.
func ParseFlags() {
	flag.Var(&PathToRessourceDirectories, "r", "the directories to look for ressources")
	flag.StringVar(&PathToGeneratedDirectory, "d", "./gen", "the directory to generate in")
	flag.BoolVar(&UseCompression, "z", true, "use compression (lz4) to store data")
	flag.UintVar(&VerboseLevel, "v", 3, "verbose level (0 to 5)")
	flag.BoolVar(&Color, "c", true, "enable color")
	flag.Parse()

	prepareSettings()
	applySettings()
}

// To prepare settings by cleaning the given parameters.
func prepareSettings() {
	if len(PathToRessourceDirectories) == 0 { // add default value
		PathToRessourceDirectories.Set("./res")
	}
	for index, dir := range PathToRessourceDirectories {
		PathToRessourceDirectories[index] = filepath.Clean(dir)
	}
	PathToGeneratedDirectory = filepath.Clean(PathToGeneratedDirectory)
}

// To check and apply the settings to the runtime.
func applySettings() {
	for _, dir := range PathToRessourceDirectories {
		fi, err := os.Stat(dir)
		if os.IsNotExist(err) == true {
			log.Error(err, "Failed to find directory \"", dir, "\"")
		} else if fi.Mode().IsDir() == false {
			log.Error(nil, "File \"", dir, "\" is not a directory")
		}
	}

	if _, err := os.Stat(PathToGeneratedDirectory); os.IsNotExist(err) == true {
		if err = os.MkdirAll(PathToGeneratedDirectory, 0755); err != nil {
			log.Error(err, "Failed to create directory \"", PathToGeneratedDirectory, "\"")
		}
	}

	log.SetLevel(VerboseLevel)
	log.SetColor(Color)
}

// To display the settings.
//
// This function is used to inform the user of the current settings.
func DisplaySettings() {
	resDir := "Source "
	if len(PathToRessourceDirectories) > 1 {
		resDir = resDir + "directories are "
	} else {
		resDir = resDir + "directory is "
	}
	for index, dir := range PathToRessourceDirectories {
		resDir = resDir + "\"" + dir + "\""
		if index+2 > len(PathToRessourceDirectories) { // last directory
			// do nothing
		} else if index+3 > len(PathToRessourceDirectories) { // 2nd last directory
			resDir = resDir + " and "
		} else {
			resDir = resDir + ", "
		}
	}
	log.Information(resDir)

	log.Information("Generated directory is \"", PathToGeneratedDirectory, "\"")

	compressionState := "enabled"
	if UseCompression == false {
		compressionState = "disabled"
	}
	log.Information("Compression is ", compressionState)
}
