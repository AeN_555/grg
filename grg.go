// Package grg is a binary to generate static Go ressources.
//
// Any file can be converted to a static ressource used by any Go package.
package main

import (
	log "gitlab.com/AeN_555/colorlog"
	"gitlab.com/AeN_555/grg/generator"
	"gitlab.com/AeN_555/grg/generator/linker"
	"gitlab.com/AeN_555/grg/settings"
	"os"
	"path/filepath"
)

// To define the entry point of the binary.
func main() {
	settings.ParseFlags()
	log.Information("Starting grg")

	settings.DisplaySettings()

	log.Information("Cleaning generated directory")
	cleanGenerated()
	log.Verbose("Generated directory cleaned")

	log.Verbose("Generating all ressources")
	for _, dir := range settings.PathToRessourceDirectories {
		generateAll(dir)
	}
	log.Information("All ressources generated")

	log.Information("Ending grg")
}

// To clean the generated folder.
//
// The goal is to avoid old unecessary (and maybe in-conflict) generated files.
func cleanGenerated() {
	if err := os.RemoveAll(settings.PathToGeneratedDirectory); err != nil {
		log.Error(err, "Failed to remove \"", settings.PathToGeneratedDirectory, "\"")
	}

	if err := os.MkdirAll(settings.PathToGeneratedDirectory, 0755); err != nil {
		log.Error(err, "Failed to make directory \"", settings.PathToGeneratedDirectory, "\"")
	}
}

// To generate all necessary files in any directories and sub-directories.
func generateAll(pathDirectory string) {
	linker.Reset()

	pathCommon := settings.PathToGeneratedDirectory + string(os.PathSeparator) + filepath.Base(settings.PathToGeneratedDirectory) + ".go"
	log.Verbose("Generating common file \"", pathCommon, "\"")
	generator.GenerateCommonFile(pathCommon)
	log.Verbose("File \"", pathCommon, "\" generated")

	err := filepath.Walk(pathDirectory,
		func(filePath string, info os.FileInfo, err error) error {
			if err != nil {
				log.Error(err, "Failed in filepath.Walk")
			}

			if info.IsDir() == true {
				log.Verbose("Generating directory for \"", filePath, "\"")
				generator.GenerateDirectory(filePath, filepath.Dir(pathDirectory))
				log.Verbose("Directory \"", filePath, "\" handled")
			} else {
				log.Information("Generating file for \"", filePath, "\" (", info.Size(), " bytes)")
				generator.GenerateFile(filePath, filepath.Dir(pathDirectory))
				log.Verbose("File \"", filePath, "\" handled")
			}
			return nil
		})
	if err != nil {
		log.Error(err, "Failed to walk through directory \"", pathDirectory, "\"")
	}

	linker.LinkFiles()
}
